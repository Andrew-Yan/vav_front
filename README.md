# VAV_front

The original VAV repo is closed source and work in progress for publication. This repo is intended to display the abstract of the original project.


**Introduction/Abstract:**

Small insertions and deletions (indels) in the human genome are the second most abundant type of mutations. These mutations can cause various human diseases including cancer. Next generation sequencing (NGS) technology allows rapid and high throughput read out of an individual's genome, and a number of computer programs have been developed to identify indels from NGS data. However, these programs suffer from low sensitivity and specificity. A post-processing procedure including manual review and refinement of indel identification is often needed in practice to achieve better accuracy. Given the massive amount of NGS raw data, this can be a daunting task.
In light of this, we have decided to develop computer programs that will allow scientists to determine if their indel candidate is indeed present in the read out of the human genome from NGS. If the indel is confirmed, further research can be carried out to determine if the indel is the causal factor to the disease of interest.

**Conclusion:**

We have developed an R package intended for Bioconductor to facilitate the indel verification process. The package scans the sequencing BAM file to calculate the frequency of the sequencing reads supporting the candidate indel and visually displays the supporting and controversial reads.